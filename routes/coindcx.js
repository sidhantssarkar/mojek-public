const { Router } = require("express");
const router = Router();
const userAuth = require("../middlewares/user");
const mongoose = require("mongoose");
const Balances = require("../models/Balances");
const Assets = require("../models/Assets");

const providerController = require("../controller/providers");
const integrationController = require("../controller/integration");
const userController = require("../controller/user");
const coindcxController = require("../controller/coindcxController");
const crypto = require("../controller/crypto");

const createAssetObject = async (holding, userID, today) => {
	const obj = {
		user: userID,
		platform: "COINDCX",
		name: holding.currency,
		amount: holding.balance,
		currency: holding.currency,
		lastUpdate: today,
	};
	return obj;
};

const createBalanceObject = async (
	holding,
	userID,
	accountID,
	providerAccount,
	today
) => {
	const obj = {
		user: userID,
		accountID,
		provider: {
			name: providerAccount.name,
			display_name: providerAccount.display_name,
			logo: providerAccount.logo,
			id: providerAccount.id,
		},
		ticker: holding.currency,
		provider_ticker: holding.currency,
		name: holding.currency,
		amount: holding.balance,
		type: "CRYPTO",
		decimals: holding.balance.split(".")[1].length,
		balanceDate: today,
		balanceUpdatedAt: new Date().getTime(),
	};
	return obj;
};

router.post("/update-holdings", userAuth, async (req, res) => {
	let userEmail = req.user.email;
	let userID = req.user._id;


	//userID from ayush's database for his testing
	// let userID = "6188eb4069b7221b3401e270";


	let providerID = req.body.providerID;
	try {
		// Get Account ID from the Integration Data
		const accountData =
			await integrationController.getAccoundIDFromProvider(
				userID,
				providerID
			);
		const accountID = accountData.accountID;
		if (!accountID) {
			return res
				.status(400)
				.send({ error: true, message: "Invalid Account Selected!" });
		}

		var today = new Date();
		var dd = String(today.getDate()).padStart(2, "0");
		var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
		var yyyy = today.getFullYear();
		today = `${yyyy}-${mm}-${dd}`;

		const providerAccount = await providerController.getProvider(
			providerID
		);
		const key = crypto.decrypt(accountData.apiKey);
		const secret = crypto.decrypt(accountData.apiSecret);

		const response = await coindcxController.getBalances(key, secret);
		const holdings = response.data.filter(
			(balance) => Number(balance.balance) !== 0
		);

		for (const holding of holdings) {
			const newBalance = await createBalanceObject(
				holding,
				userID,
				accountID,
				providerAccount,
				today
			);
			const newAsset = await createAssetObject(holding, userID, today);

			var assetObj = await Assets.findOne({
				user: userID,
				platform: "COINDCX",
				name: holding.currency,
			});
			var balanceObj = await Balances.findOne({
				user: userID,
				accountID,
				balanceDate: today,
				name: holding.currency,
			});
			if (!balanceObj) {
				// if balance obj doesn't exist
				await Balances.create(newBalance);
			} else {
				Object.assign(balanceObj, newBalance);
				await balanceObj.save();
			}

			if (!assetObj) {
				await Assets.create(newAsset);
			} else {
				Object.assign(assetObj, newAsset);
				await assetObj.save();
			}
		}
		res.send({ status: true });
	} catch (error) {
		res.status(400).send({
			error: true,
			message:
				"Unable to update holdings at the moment. Please try again later.",
		});
	}
});

module.exports = router;

