const CryptoJS = require("crypto-js");
const axios = require("axios");

const getTransactions = async (apiKey, secretKey) => {
  return new Promise(async (resolve, reject) => {
    try {
      const baseUrl = "https://api.wazirx.com";
      const timestamp = new Date().getTime();
      const payload = `recvWindow=20000&symbol=btcusdt&timestamp=${timestamp}`;
      const signature = CryptoJS.HmacSHA256(payload, secretKey).toString();

      var config = {
        method: "get",
        url: `${baseUrl}/sapi/v1/allOrders?${payload}&signature=${signature}`,
        headers: {
          "X-Wx-Apikey": apiKey,
        },
      };
      const response = await axios(config);
      resolve(response);
    } catch (error) {
      reject(error);
    }
  });
};

const getBalances = async (apiKey, secretKey) => {
  return new Promise(async (resolve, reject) => {
    try {
      const baseUrl = "https://api.wazirx.com";
      const timestamp = new Date().getTime();
      const payload = `recvWindow=20000&timestamp=${timestamp}`;
      const signature = CryptoJS.HmacSHA256(payload, secretKey).toString();

      var config = {
        method: "get",
        url: `${baseUrl}/sapi/v1/funds?${payload}&signature=${signature}`,
        headers: {
          "X-Api-Key": apiKey,
        },
      };
      const response = await axios(config);
      resolve(response);
    } catch (error) {
      reject(error);
    }
  });
};

const getUserInfo = async (apiKey, secretKey) => {
  return new Promise(async (resolve, reject) => {
    try {
      const baseUrl = "https://api.wazirx.com";
      const timestamp = new Date().getTime();
      const payload = `recvWindow=20000&timestamp=${timestamp}`;
      const signature = CryptoJS.HmacSHA256(payload, secretKey).toString();

      var config = {
        method: "get",
        url: `${baseUrl}/sapi/v1/account?${payload}&signature=${signature}`,
        headers: {
          "X-Api-Key": apiKey,
        },
      };
      const response = await axios(config);
      resolve(response);
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = {
  getTransactions,
  getBalances,
  getUserInfo,
};
