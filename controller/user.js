const Integration = require("../models/Integrations");
const crypto = require("../controller/crypto");

const getIntegrations = (userID) => {
    return new Promise((resolve, reject) => {
        Integration.find({
            user: userID
        }).exec((err, integrations) => {
            if (err) {
                return reject(err);
            }
            resolve(integrations);
        })
    })
}

const getIntegration = (match) => {
    return new Promise((resolve, reject) => {
        Integration.findOne(match).exec((err, integration) => {
            if (err) {
                return reject(err)
            }
            resolve(integration)
        })
    })
}

const updateWallets = (userID, userEmail) => {
    return new Promise(async (resolve, reject) => {
        try {
            const integrations = await getIntegrations(userID);
            let vezgoUpdated = false;
            for (let i = 0; i < integrations.length; i++) {
                const integration = integrations[i];

                // Based on the integration provider, get data from the provider and update

            }
            resolve()
        } catch (error) {
            reject(error)
        }
    })
}

module.exports = {
    getIntegrations,
    getIntegration,
    updateWallets
}