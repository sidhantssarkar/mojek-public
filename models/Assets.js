const mongoose = require("mongoose");
var Float = require('mongoose-float').loadType(mongoose);

var assetSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
        required: true
    },
    platform: {
        type: String,
        enum: ["BUC", "ZERODHA", "COINDCX"],
        required: true
    },
    currency: {
        type: String,
        required: true
    },
    amount: {
        type: Float,
        required: true
    },
    logo: {
        type: String
    },
    name: {
        type: String
    },
    lastUpdate: {
        type: String
    },
    value: {
        type: Float
    }
});

assetSchema.set("timestamps", true);

const Asset = mongoose.model("assets", assetSchema);

module.exports = Asset;
