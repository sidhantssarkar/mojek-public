const mongoose = require("mongoose");
const validator = require("validator");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
var userSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        required: true
    },
    email: {
        type: String,
        trim: true,
        lowercase: true,
        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error("Email is invalid");
            }
        },
        unique: true,
        index: true
    },
    phone: {
        iv: {
            type: String
        },
        content: {
            type: String
        }
    },
    passcode: {
        iv: {
            type: String
        },
        content: {
            type: String
        }
    },
    password: {
        type: String,
        required: true,
        minlength: 6,
        trim: true,
        validate(value) {
            if (value.toLowerCase().includes("password")) {
                throw new Error('Password cannot contain "password"');
            }
        },
    },
    tokens: [
        {
            token: {
                type: String,
                required: true,
            },
        },
    ],
    loginTokens: [
        {
            token: {
                type: String,
                required: true,
            },
        },
    ],
    lastBalanceFetched: {
        type: Date
    }
});

userSchema.methods.generateAuthToken = async function () {
    const user = this;
    const token = jwt.sign({ _id: user._id.toString(), expiresIn: "3d" }, process.env.JWT_SALT);
    user.tokens = user.tokens.concat({ token });
    await user.save();

    return token;
};

userSchema.methods.generateLoginInitToken = async function () {
    const user = this;
    const token = jwt.sign({ _id: user._id.toString(), expiresIn: "1m" }, process.env.JWT_SALT);
    user.loginTokens = user.loginTokens.concat({ token });
    await user.save();

    return token;
};

userSchema.methods.getPublicProfile = function () {
    const user = this;
    const userObject = user.toObject();

    delete userObject.password;
    delete userObject.tokens;

    return userObject;
};

userSchema.statics.findByEmailCredentials = async (email, password) => {
    const user = await User.findOne({ email });
    if (!user) {
        throw new Error("Unable to login");
    }
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
        throw new Error("Unable to login");
    }
    return user;
};

userSchema.statics.findByPhoneCredentials = async (phone, password) => {
    const user = await User.findOne({ phone });
    if (!user) {
        throw new Error("Unable to login");
    }
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
        throw new Error("Unable to login");
    }
    return user;
};

userSchema.pre("save", async function (next) {
    const user = this;

    if (user.isModified("password")) {
        user.password = await bcrypt.hash(user.password, 8);
    }

    next();
});

userSchema.set("timestamps", true);

const User = mongoose.model("user", userSchema);

module.exports = User;